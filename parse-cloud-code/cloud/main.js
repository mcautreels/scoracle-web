Parse.Cloud.job("updateScores", function(request, response) {
        Parse.Cloud.useMasterKey();
         
        var Match = Parse.Object.extend("Match");
        var Prediction = Parse.Object.extend("Prediction");
         
        var matchId = request.params.matchId;
         
        var matchQuery = new Parse.Query(Match);
        matchQuery.get(matchId, {
            success: function(match){
                var matchHomeGoals = match.get("homeGoals");
                var matchAwayGoals = match.get("awayGoals");
             
                var predictionsQuery = new Parse.Query(Prediction);
                predictionsQuery.equalTo("matchId", matchId);
                predictionsQuery.find({
                    success: function(predictions){
                        var predictionCount = predictions.length;
                        var saveIteration = 0;
                     
                        predictions.forEach(function(prediction) {
                            var predictionHomeGoals = prediction.get("homePrediction");
                            var predictionAwayGoals = prediction.get("awayPrediction");
                             
                            var score = calculateScore(matchHomeGoals, matchAwayGoals, predictionHomeGoals, predictionAwayGoals)
                             
                            prediction.set("score", score);
                            prediction.save(null, {
                                success: function(prediction) {
                                    var userQuery = new Parse.Query(Parse.User);
                                    userQuery.get(prediction.get("userId"), {
                                        success: function(user) {
                                            user.increment("totalScore", score);
                                            user.save(null, {
                                                success: function(user) {
                                                    saveIteration++;
                                                     
                                                    console.log("Saved score: " + saveIteration + "/" + predictionCount);
                                                     
                                                    if(predictionCount == saveIteration) {
                                                        match.set("status", "PLAYED");
                                                        match.save(null, {
                                                            success: function(match) {
                                                                response.success("Success!");
                                                            },
                                                            error: function(error) {
                                                                handleError(error);
                                                            }
                                                        });
                                                    }
                                                },
                                                error: function(error) {
                                                    handleError(error);
                                                }
                                            });
                                        },
                                        error: function(object, error) {
                                            handleError(error);
                                        }
                                    });
                                },
                                error: function(error){
                                    handleError(error);
                                }
                            });
                        });
                    },
                    error: function(error){
                        handleError(error);
                    }
                }); 
            },
            error: function(error){
                handleError(error);
            }
        });
         
        var handleError = function(error) {
            response.error(error);
        }
         
        function calculateScore(matchHomeGoals, matchAwayGoals, predictionHomeGoals, predictionAwayGoals) {
            var score = 0;
            var homePredicted = false;
            var awayPredicted = false;
             
            if(matchHomeGoals == predictionHomeGoals) {
                score += 10;
                homePredicted = true;
            }
             
            if(matchAwayGoals == predictionAwayGoals) {
                score += 10;
                awayPredicted = true;
            }
             
            if(homePredicted && awayPredicted) {
                score += 10;
            }
             
            return score;
        }
    }
);
 
Parse.Cloud.job("resetAllMatchStatus", function(request, response) {
    Parse.Cloud.useMasterKey();
         
    var Match = Parse.Object.extend("Match");
     
    var matchQuery = new Parse.Query(Match);
    matchQuery.find({
        success: function(matches){
            var matchesCount = matches.length;
            var matchesIteration = 1;
            console.log("Matches count: " + matchesCount);
             
            matches.forEach(function(match) {
                match.set("status", "UNPLAYED");
                match.save(null, {
                    success: function(match) {
                        console.log("Updated match: " + matchesIteration++ + "/" + matchesCount);
                    },
                    error: function(error){
                        response.error(error);
                    }
                });
            });
        },
        error: function(error){
            response.error(error);
        }
    });
});

Parse.Cloud.beforeSave("Prediction", function(request, response) {
    var query = new Parse.Query("Match");
    query.get(request.object.get("matchId"), {
        success: function(match) {
            if(new Date(match.get("dateTime")) <= new Date()) {
                response.error("Match has been played, no more predictions allowed!");
            } else {
                response.success();
            }
        },
        error: function(object, error) {
            response.error("Match not found");
        }
    });
});