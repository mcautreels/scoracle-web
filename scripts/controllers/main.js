'use strict';

var user = JSON.parse(localStorage.getItem('user'));
var defaultHeaders = {
    'X-Parse-Application-Id': 'ie6B89kzd0YNOZk7n7uZQedP7yApUyo2DZCfqsF5',
    'X-Parse-REST-API-Key': 'MlczqEJfBT013Jp37qNBMpJFplV3EzKpEkU6AQLh'
}

angular.module('scoracleApp')
  .controller('MainCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) {
    if(user != undefined) {
      $location.path("/predictions");
    }

    $scope.login = function() {
      $http({
        method: 'GET', 
        url: 'https://api.parse.com/1/login',
        headers: defaultHeaders,
        params: {
          username: $scope.username,
          password: $scope.password
        }
      }).
      success(function(data, status, headers, config) {
        user = data;
        localStorage.setItem('user', JSON.stringify(user));
        $location.path("/predictions")
      }).
      error(function(data, status, headers, config) {
        $scope.message = 'Login Failed';
        $scope.technicalMessage = data.error;
      });
    }
  }])
  .controller('ScheduleCtrl', ['$rootScope', '$scope', 'Match',
    function ($rootScope, $scope, Match) {
      $scope.matches = Match.query({order: "datetime"});

      $scope.loadPrediction = function(matchId, matchDate) {
        console.log('Load Prediction for Match: ', matchId);
        if(new Date(matchDate) > new Date()) {
          $scope.$broadcast('CURRENT_MATCH_ID', matchId);
        }        
      }
    }
  ])
  .controller('PredictionCtrl', ['$scope', 'Prediction', '$location',
    function ($scope, Prediction, $location) {
      if(user == undefined) {
        $location.path("/home");
        return;
      }

      $scope.isActivePrediction = false;

      $scope.matchPrediction = {
        homePrediction: 0,
        awayPrediction: 0,
        userId: user.objectId
      }

      $scope.$on('CURRENT_MATCH_ID', function(event, matchId) {
        if($scope.$parent.match.objectId == matchId) {
          Prediction.query({where: '{"matchId": "' + matchId + '","userId": "' + user.objectId + '"}'}, function(result) {
            if(result.results.length > 0) {
              $scope.matchPrediction = result.results[0];
            }
            $scope.isActivePrediction = true;
            $scope.matchPrediction.matchId = matchId;
          });
        }
      })

      $scope.addHome = function() {$scope.matchPrediction.homePrediction++;}
      $scope.subtractHome = function() {
        if($scope.matchPrediction.homePrediction > 0) {
          $scope.matchPrediction.homePrediction--;
        }
      }
      $scope.addAway = function() {$scope.matchPrediction.awayPrediction++;}
      $scope.subtractAway = function() {
        if($scope.matchPrediction.awayPrediction > 0) {
          $scope.matchPrediction.awayPrediction--;
        }
      }

      $scope.save = function() {
        console.log('Save the Prediction');

        if($scope.matchPrediction.objectId != undefined) {
          // Update
          Prediction.update({objectId: $scope.matchPrediction.objectId}, $scope.matchPrediction);
        } else {
          // Insert
          Prediction.insert($scope.matchPrediction);
        }

        $scope.isActivePrediction = false;
      }
    }
  ])  
  .controller('LeaguesCtrl', ['$scope', '$location', '$route', 'League', 'Member', function ($scope, $location, $route, League, Member) {
    League.query({where: '{"objectId":{"$select":{"query":{"className":"Member","where":{"userId":"' + user.objectId + '", "accepted": true}},"key":"leagueId"}}}'}, function (result) {
      if(result.results.length > 0) {
        $scope.leagues = result.results;
      }
    });
    League.query({where: '{"objectId":{"$select":{"query":{"className":"Member","where":{"userId":"' + user.objectId + '", "accepted": false}},"key":"leagueId"}}}'}, function (result) {
      if(result.results.length > 0) {
        $scope.invites = result.results;
      }
    });

    $scope.acceptInvite = function (leagueId) {
      Member.query({where: '{"leagueId": "' + leagueId + '", "userId": "' + user.objectId + '"}'}, function (result) {
        var member = result.results[0];
        member.accepted = true;
        Member.update({objectId: member.objectId}, member);
        $route.reload();
      });
    }
  }])
  .controller('LeagueCtrl', ['$scope', '$location', '$routeParams', '$route', 'League', 'Member', 'User', function ($scope, $location, $routeParams, $route, League, Member, User) {
    $scope.league = League.query({objectId: $routeParams.leagueId});
    Member.query({where: '{"leagueId": "' + $routeParams.leagueId + '"}'}, function(result) {
      $scope.members = result.results;
    });

    $scope.isManager = function() {
      if($scope.league.userId == user.objectId) {
        return true;
      }
      return false;
    }

    $scope.addUser = function() {
      var username = $scope.username;

      User.query({where: '{"$or":[{"username":"' + username + '"},{"email": "' + username + '"}]}'}, function (result) {
        var user = result.results[0];

        if(user != undefined) {
          var member = {
            leagueId: $scope.league.objectId,
            userId: user.objectId,
            accepted: false,
            username: user.username
          }
          Member.insert(member, function() {
            $route.reload();
          });
        } else {
          $scope.message = 'User could not be found';
        }
      });
    }

    $scope.removeMember = function (memberId) {
      Member.remove({objectId: memberId}, function () {
        $route.reload();
      });
    }
  }])
  .controller('AboutCtrl', function ($scope) {

  })
  .controller('ContactCtrl', function ($scope) {

  })
  .controller('RegisterCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) {
    $scope.register = function() {
      if($scope.password != $scope.password2) {
        $scope.message = 'Please enter your password twice';
      } else {
        $http({
          method: 'POST', 
          url: 'https://api.parse.com/1/users',
          headers: defaultHeaders,
          data: {
            username: $scope.username,
            password: $scope.password,
            email: $scope.email
          }
        }).
        success(function(data, status, headers, config) {
          console.log(data);
          $location.path( "/home" );
        }).
        error(function(data, status, headers, config) {
          $scope.message = 'Login Failed';
          $scope.technicalMessage = data.error;
        });
      }
    } 
  }])
  .controller('NavigationCtrl', ['$scope', '$location', function($scope, $location) {
    $scope.isActive = function(path) {
      if ($location.path().substr(0, path.length) == path) {
        return true;
      } else {
        return false;
      }
    }
  }]);