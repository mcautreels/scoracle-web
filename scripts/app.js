'use strict';

var app = angular
  .module('scoracleApp', [
    'ngCookies',
    'ngResource',
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/home', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/home/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl'
      })
      .when('/predictions', {
        templateUrl: 'views/predictions.html',
        controller: 'ScheduleCtrl'
      })
      .when('/leagues', {
        templateUrl: 'views/leagues.html',
        controller: 'LeaguesCtrl'
      })
      .when('/leagues/:leagueId', {
        templateUrl: 'views/league.html',
        controller: 'LeagueCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/home'
      });
  });


jQuery(document).ready(function($) {
  resizeCircles();
  $(window).resize(resizeCircles);

  $('.match-time').on('click', '.circle', function() {
    $('.match-prediction').hide();
    $(this).parent().parent().show();
  });
});

// Resize circles
var resizeCircles = function() {
  $('.circle').each(function() {
    var circle = $(this);

    var circleWidth = $(circle).width();
    $(circle).css('height', circleWidth);
    $(circle).css('border-radius', circleWidth / 2)
    $(circle).css('padding-top', circleWidth / 4)
  });

  window.setTimeout('resizeCircles()', 50);
}