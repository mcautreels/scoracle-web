var matchUrl = 'https://api.parse.com/1/classes/Match/:_objectId';
var predictionUrl = 'https://api.parse.com/1/classes/Prediction/:objectId';
var leagueUrl = 'https://api.parse.com/1/classes/League/:objectId';
var memberUrl = 'https://api.parse.com/1/classes/Member/:objectId';
var userUrl = 'https://api.parse.com/1/users/:objectId';


var defaultHeaders = {
    'X-Parse-Application-Id': 'ie6B89kzd0YNOZk7n7uZQedP7yApUyo2DZCfqsF5',
    'X-Parse-REST-API-Key': 'MlczqEJfBT013Jp37qNBMpJFplV3EzKpEkU6AQLh'
}

var module = angular.module('scoracleApp').provider('Match', function() {
    this.$get = ['$resource', function($resource) {
        var Match = $resource(matchUrl, {}, {
            query: { 
                headers: defaultHeaders
            }
        })

        return Match;
    }]
}).provider('Prediction', function() {
    this.$get = ['$resource', function($resource) {
        var Prediction = $resource(predictionUrl, {}, {
            query: { 
                headers: defaultHeaders
            },
            update: {
                method: 'PUT',
                headers: defaultHeaders
            },
            insert: {
                method: 'POST',
                headers: defaultHeaders
            }
        })

        return Prediction;
    }]
})
.provider('League', function() {
    this.$get = ['$resource', function($resource) {
        var Match = $resource(leagueUrl, {}, {
            query: { 
                headers: defaultHeaders
            }
        })

        return Match;
    }]
})
.provider('Member', function() {
    this.$get = ['$resource', function($resource) {
        var Member = $resource(memberUrl, {}, {
            query: { 
                headers: defaultHeaders
            },
            update: {
                method: 'PUT',
                headers: defaultHeaders
            },
            insert : {
                method: 'POST',
                headers: defaultHeaders
            },
            remove : {
                method: 'DELETE',
                headers: defaultHeaders
            }
        })

        return Member;
    }]
})
.provider('User', function() {
    this.$get = ['$resource', function($resource) {
        var User = $resource(userUrl, {}, {
            query: { 
                headers: defaultHeaders
            }
        })

        return User;
    }]
});